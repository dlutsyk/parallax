(function ($) {
    $.fn.parallax = function (options) {
        
        this.each(function () {
            var $this = $(this);
            $this
            .css({
                "min-height" : "10px",
                "position"   : "relative",
                "overflow"   : "hidden"
            })
            .wrapInner("<div class='parallax-content' style='position: relative;z-index:1;'>")
            .prepend("<div class='image-parallax' style='position:absolute;top:0;width:100%;background-image:url("+ $this.data('parallax-image') +");background-size:cover;background-position:top;'>");
            
            function parallaxInit() {
                
                var pheight = $this.height(),
                    st = $(window).scrollTop(),
                    sp = $this.offset().top - $(window).height(),
                    sr = st-sp,
                    ob = $this.offset().top+pheight,
                    imgParallax = $this.children(".image-parallax");
                
                if(st >= sp && st <= ob){
                    imgParallax.css({
                        "transform" : "translate3d(0px, "+ sr / 20 +"%, 0px)",
                         "-webkit-transform" : "-webkit-translate3d(0px, "+ sr / 20 +"%, 0px)"
                    });
                }
                
                imgParallax.css({
                    "height" : pheight*2,
                    "top"    : -pheight
                });
            };
            
            $(window).scroll(function() {
                parallaxInit();
            }).load(function() {
                parallaxInit();
            });
            
            $("*").resize(function() {
                parallaxInit();
            });
            
        });
    };
})(jQuery);